'use strict';

document.addEventListener("DOMContentLoaded", function() {

    // SVG IE11 support
    svg4everybody();

    $('.nav-toggle').on("click", function (e) {
        e.preventDefault();
        $('.page').toggleClass('nav-open');
    });

    $('.header__nav--item').on("click", function (e) {
        e.preventDefault();
        $('.page').removeClass('nav-open');

    });

    
    let anchors = document.querySelectorAll('.btn-scroll')
    for (let anchor of anchors) {
        anchor.addEventListener('click', function (e) {
            e.preventDefault()
            
            let blockID = anchor.getAttribute('href').substr(1)
            
            document.getElementById(blockID).scrollIntoView({
            behavior: 'smooth',
            block: 'start'
            })
        })
    }


    $('.accordion__header').on("click", function (e) {
        e.preventDefault();
        let box = $(this).closest('.accordion__item');
        box.toggleClass('open');
        box.find('.accordion__content').slideToggle('fast');

    });
});
